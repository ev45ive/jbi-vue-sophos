## Git
cd ..
git clone https://bitbucket.org/ev45ive/jbi-vue-sophos.git jbi-vue-sophos
cd jbi-vue-sophos/vue-spa
npm i 
npm run serve

## Git update
git stash -u
git pull
npm i 



## INstallations 
node -v 
v14.17.0

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

code -v 
1.58.2

chrome://version 91

## CLI
https://cli.vuejs.org/guide/creating-a-project.html#vue-create

npm i -g @vue/cli 
C:\Users\PC\AppData\Roaming\npm\vue -> C:\Users\PC\AppData\Roaming\npm\node_modules\@vue\cli\bin\vue.js

Installed CLI Plugins
babel pwa router vuex eslint unit-jest typescript
vue-router vuex vue-devtools vue-loader awesome-vue

## PLugings
https://marketplace.visualstudio.com/items?itemName=octref.vetur 

https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd

https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets

mkdir -p src/views/playlists
touch src/views/playlists/Playlists.vue
mkdir -p src/components/playlists
touch src/components/playlists/PlaylistDetails.vue
touch src/components/playlists/PlaylistEditForm.vue
touch src/components/playlists/PlaylistList.vue

## lifecycle
https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram


## Search
mkdir -p src/views/search

mkdir -p src/components/search
mkdir -p src/components/forms
mkdir -p src/components/albums

touch src/views/search/Search.vue
touch src/components/forms/SearchForm.vue
touch src/components/search/SearchResults.vue
touch src/components/albums/AlbumCard.vue

touch src/core/model/Search.ts

## Mocking
https://github.com/marak/Faker.js/
https://github.com/boo1ean/casual


## Http / Axios
npm install --save axios vue-axios

## Validation
https://github.com/colinhacks/zod
https://github.com/gvergnaud/ts-pattern#__-wildcard


https://github.com/typestack/class-transformer
https://github.com/typestack/class-validator


## Vuex class modules
https://github.com/gertqin/vuex-class-modules