import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import SearchView from '../views/search/Search.vue'
import PlaylistsView from '../views/playlists/Playlists.vue'
Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    redirect: '/search'
  },
  {
    path: "/search",
    name: "Search",
    component: SearchView,
  },
  {
    path: "/playlists",
    name: "Playlists",
    component: PlaylistsView,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
