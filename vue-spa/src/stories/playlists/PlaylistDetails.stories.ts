import PlaylistDetails from '../../components/playlists/PlaylistDetails.vue'
import { Story } from '@storybook/vue'

export default {
  title: 'Playlists/Details',
  component: PlaylistDetails,
};

const Template: Story = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PlaylistDetails },
  template:
    '<PlaylistDetails />',
});

export const Primary = Template.bind({});
Primary.args = {
  playlist: {},
};
