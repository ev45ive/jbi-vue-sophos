import { Playlist } from "@/core/model/Playlist";
import Vue from "vue";
import Vuex, { ModuleTree } from "vuex";
import { RootState } from "./RootState";

Vue.use(Vuex);

export const store = new Vuex.Store<RootState>({
  state: {
    counter: 0
  },
  mutations: {
    increment(state, amount: number) {
      state.counter
    }
  },
  actions: {},
  modules: {
    // playlists
  },
});

export default store


