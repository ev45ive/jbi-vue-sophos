import { Playlist } from "@/core/model/Playlist";
import { store } from "./index";
import { RootState } from "./RootState";
import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import axios from "axios";
import { getToken } from "@/core/services/Auth";
import { PagingObject } from "@/core/model/Search";

export interface PlaylistsState {
    items: Playlist[]
}


@Module
class UserModule extends VuexModule {
    // state 
    items: Playlist[] = []

    // getters
    get playlists() {
        return this.items
    }

    // mutations
    @Mutation
    setPlaylists(data: Playlist[]) {
        this.items = data;
    }


    // actions
    @Action
    async loadUser() {
        const { data } = await axios.get<PagingObject<Playlist>>(`me/playlists`, {
            headers: {
                Authorization: 'Bearer ' + getToken()
            }
        })
        this.setPlaylists(data.items)
    }
}

export const playlistsModule = new UserModule({ store, name: "playlists" });

// export const playlists: Module<PlaylistsState, RootState> = {
//     state: {
//         items: []
//     },
//     mutations: {
//         loadPlaylists(state, data: Playlist[]) {
//             state.items = data;
//         }
//     },
//     actions: {},
//     modules: {},
// };
