import { RootState } from "./store/RootState";

declare module "vue/types/vue" {
  interface Vue {
    $store: Store<RootState>;
  }
}