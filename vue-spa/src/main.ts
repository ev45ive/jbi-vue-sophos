import Vue from "vue";

import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

/// <reference path="./index.d.ts" />

Vue.config.productionTip = false;

(window as any).app = new Vue({
  // el:"#app"
  // template: '<h1></h1>', // pre-compile the templates into render functions, or use the compiler-included build.
  router,
  store,
  // render(h) {
    // Free Monad
    // return h('h1', { style: 'color:red;' }, 'Hello Virtual DOM'),
    // return h(() => ({
    //   render() {
    //     return h('h1', { style: 'color:red;' }, 'Hello Virtual DOM')
    //   }
    // }))
  // }
  render: (h) => h(App),
}).$mount("#app");
