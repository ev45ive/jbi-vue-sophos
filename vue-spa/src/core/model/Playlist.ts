interface Entity{
    id: string
    name: string
}

export interface Track extends Entity {
    readonly type: 'track'
    duration: number
}

export interface Playlist  extends Entity{
    readonly type: 'playlist'
    public: boolean;
    description: string;
    tracks?: Track[]
}
