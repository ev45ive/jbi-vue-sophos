import axios, { AxiosError } from "axios"
import { Album, assertSpotifyError, SearchResponse } from "../model/Search";
import { getToken } from "./Auth";

axios.defaults.baseURL = 'https://api.spotify.com/v1/'



export const fetchSearchResults = async (query: string): Promise<Album[]> => {
    try {
        const { data } = await axios.get(`search`, {
            headers: {
                Authorization: 'Bearer ' + getToken()
            },
            params: {
                type: 'album', q: query
            }
        })
        assertSearchResponse(data)

        return data.albums.items
    } catch (err) {
        console.error(err);

        assertSpotifyError(err)

        throw new Error(err.response?.data.error.message)
    }
}

export function assertSearchResponse(res: any): asserts res is SearchResponse<Album> {
    if (!(res?.albums?.items instanceof Array)) {
        throw new Error('Invalide server response')
    }
}