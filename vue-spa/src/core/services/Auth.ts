
let token: string | null = null

export const getToken = () => token;

export const login = () => {
    const url = 'https://accounts.spotify.com/authorize'

    const params = new URLSearchParams({
        client_id: '1421c192a47b4e98a14fc62e7a5929bc',
        response_type: 'token',
        redirect_uri: 'http://localhost:8080/',
        scope: 'playlist-modify-public playlist-modify-private playlist-read-private playlist-read-collaborative'
    })

    window.location.href = (`${url}?${params}`);
}

export const logout = () => {
    token = null
}

export const initAuth = () => {
    if (!token) {
        token = new URLSearchParams(window.location.hash).get('#/access_token');
    }
    if (token) {
        window.location.hash = ''
        sessionStorage.setItem('token', JSON.stringify(token))
    }
    if (!token) {
        const rawToken = sessionStorage.getItem('token');
        token = rawToken && JSON.parse(rawToken)
    }
    if (!token) {
        login()
    }
}