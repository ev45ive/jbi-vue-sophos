```js
obj = {
    _secret: '',
    get secret(){
        return this._secret.split('').reverse().join('')
    },
    set secret(val){
        this._secret = val.split('').reverse().join('')
    }
}
{_secret: ""}
obj.secret = 'I love VueJS'
// "I love VueJS"

obj 
// {_secret: "SJeuV evol I"}

obj.secret
// "I love VueJS"
```