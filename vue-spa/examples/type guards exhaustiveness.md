```ts
import { Playlist, Track } from '../src/core/model/Playlist'

function renderSearchResult(result: Playlist | Track) {
    // if(result.type === 'playlist'){
    //     result.tracks?.length
    // }
    switch (result.type) {
        case 'playlist': return `${result.name} - tracks ${result.tracks?.length || 0}`
        case 'track': return `${result.name} - duration ${result.duration} ms`
        default: assertExhaustiveness(result)
    }
}

/**
 * Get Track info
 * @param playlist Playlist
 */
function getTracksInfo(playlist: Playlist) {
    if (playlist.tracks) { const len0 = playlist.tracks.length; }
    const len1 = playlist.tracks ? playlist.tracks.length : 0
    const len2 = playlist.tracks?.length || 0
    const len3 = playlist.tracks?.length ?? 0
    const len4 = playlist.tracks?.length
    // Hack / Risky
    const len5 = playlist.tracks!.length
    const len6 = (playlist.tracks as Track[]).length
}


type MaybeString = string | number // | boolean

function renderResultToString(result: MaybeString): string {
    // result.toString()

    if (typeof result === 'string') {
        return result.toLocaleUpperCase()
    } else if (typeof result === 'number') {
        return result.toFixed(2)
    } else {
        // return result
        // check exhaustiveness
        // const _unexpected: never = result
        // throw new Error('Unexpected value ' + result)
        assertExhaustiveness(result)
        // const x = 1 // Unreachable code detected.
    }
}

const render = renderResultToString({} as any)
render.toUpperCase()

function assertExhaustiveness(result: never): never {
    throw new Error('Unexpected value ' + result)
}

// Structural (not Nominal) type checking

interface Vector { x: number; y: number, length: number }
interface Point { x: number; y: number }

let v: Vector = { x: 123, y: 133, length: 123 }
let p: Point = { x: 123, y: 133 }

// v = p; // Property 'length' is missing in type 'Point' but required in type 'Vector'.
p = v;

// p.length // Property 'length' does not exist on type 'Point'
(p as Vector).length

// p['color'] = 123 //   Property 'color' does not exist on type 'Point'


type Scalar = Vector | Point

function draw(scalar: Scalar) {
    // if (p.length) { } //  Property 'length' does not exist on type 'Point'
    scalar // Scalar
    if ('length' in scalar) {
        scalar.length // Vector
    } else {
        scalar // Point
    }
}

function getTime(time: Date | string) {
    if (time instanceof Date) {
        time.toTimeString()
    } else {
        new Date(time).toTimeString()
    }
}


// Indexed types

const obj = {
    [1 + 2 + 3]: 123
}
interface Dict {
    [term: string]: string
}
const dict: Dict = { '123': '123' }
dict['234'] = '234'

    // export class Playlist {
    //     id!: string;
    //     name!: string;
    //     public!: boolean;
    //     description!: string;

    //     // private constructor() { }

    //     // constructor(
    //     //     public id: string,
    //     //     public name: string,
    //     //     public public: boolean,
    //     //     public description: string,
    //     // ) { }
    // }

    // p1 = new Playlist() 
    // p1 instanceof Playlist == true
    // p1.__proto__.constuctor == Playlist 

    // var p2: Playlist = {}
    // p1.__proto__ == Object
    // p1 instanceof Playlist == false

    // https://github.com/typestack/class-transformer

    ```